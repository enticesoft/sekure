﻿Feature: QAPortalLogin
	In order to use QA Outsourcing portal
	As a user
	I want to Login in to application

@TC0001 @smoke
Scenario: QA Outsourcing Portal - Verify that system allow user to login into application with valid credentials
	Given I am on QA Outsourcing Portal Login page
	When I enter valid UserName & Password
	| UserName   | Password   |
	| out-user-1 | QaUser123. |
	And I click on the Login button 
	Then I should login into QA Outsourcing Portal
	And I should logout from application

@TC0002 @smoke
Scenario: QA Outsourcing Portal - Verify that system not allow user to login with invalid credentials
	Given I am on QA Outsourcing Portal Login page
	When I enter valid UserName & Password
	| UserName   | Password   |
	| Manoj      | Manojb123. |
	And I click on the Login button 
	Then I should see Invalid username or password message