﻿Feature: QA Portal Customers
	In order to Check Details on Pending Email Page
	As a user 
	I want to be on Pending Emails Page

@TC0003 @smoke
Scenario: Verify that when user open Customer details page and click on the 'Customer List' Button then system should open Customer Details page
	Given I am on QA Outsourcing Portal Login page
	When I enter valid UserName & Password
	| UserName   | Password   |
	| out-user-1 | QaUser123. |
	And I click on the Login button
	And I Open Customer Details page by clicking on the Opener Name
	And I click on the Customer List Button
	Then I should see Customer List page
	And I should logout from application

@TC0004 @smoke
Scenario: Verify that when user open Customer details page then Opener Name should be correct under Call Listing 
	Given I am on QA Outsourcing Portal Login page
	When I enter valid UserName & Password
	| UserName   | Password   |
	| out-user-1 | QaUser123. |
	And I click on the Login button
	And I Open Customer Details page by clicking on the Opener Name
	Then I should see correct Opener name on Customer Details Page
	And I should logout from application

@TC0005 @smoke
Scenario: Verify that when user submit Unable to Assess requrest then system should show records on CRM->Pending Email page
	Given I am on QA Outsourcing Portal Login page
	When I enter valid UserName & Password
	| UserName   | Password   |
	| out-user-1 | QaUser123. |
	And I click on the Login button
	And I Open Customer Details page by clicking on the Opener Name
	And I click on Unable to Assess button 
	And I Enter data in Reason text box
	| Reason |
	|Qa Test |
	And I click on the Send Email button
	And I logout from the QA Outsourcing Portal
	And I navigate to the Sekure CRM application URL
	And I login into Sekure CRM application
	| CRMUserName   | CRMPassword  |
	| manojb        | bmanoj321    |
	And I navigate to Pending Email Page
	And I search customer for which Unable to Assess request submitted
	Then I should see customer in the list for which Unable to Assess request submitted