﻿using System;
using System.Collections.Generic;
using System.Linq;
using SekureFramework;
using NUnit.Framework;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SekureTests.Steps
{
    [Binding]
    public class QAPortalCustomersSteps
    {
        [When(@"I Open Customer Details page by clicking on the Opener Name")]
        public void WhenIOpenCustomerDetailsPageByClickingOnTheOpenerName()
        {
            Pages.CustomerPage.clickOnOpenerNameLink();
        }

        [Then(@"I should see correct Opener name on Customer Details Page")]
        public void ThenIShouldCorrectOpenerNameOnCustomerDetailsPage()
        {
            Boolean OpenerNameB = Pages.CustomerPage.checkcorrectOpenerNameDisplayed();
            Assert.IsTrue(OpenerNameB, "User can see correct Opener name on Customer Details. ");
        }

        [When(@"I click on the Customer List Button")]
        public void WhenIClickOnTheCustomerListButton()
        {
            Pages.CustomerPage.clickOnCustomerListBtn();
        }

        [Then(@"I should see Customer List page")]
        public void ThenIShouldSeeCustomerListPage()
        {
            Boolean CustomersPage = Pages.CustomerPage.checkCustomerListPageDisplayed();
            Assert.IsTrue(CustomersPage, "User can see Customer Page. ");
        }

        [When(@"I click on Unable to Assess button")]
        public void WhenIClickOnUnableToAssessButton()
        {

            Pages.CustomerPage.clickOnUnabletoAssessBtn();
        }

        [When(@"I Enter data in Reason text box")]
        public void WhenIEnterDataInReasonTextBox(Table table)
        {
            
             String reason = table.Rows[0]["Reason"];
             Pages.CustomerPage.enterReasononthePopup(reason);
            
        }

        [When(@"I click on the Send Email button")]
        public void WhenIClickOnTheSendEmailButton()
        {
            Pages.CustomerPage.clickOnSendEmailBtn();
        }

        [When(@"I navigate to the Sekure CRM application URL")]
        public void WhenINavigateToTheSekureCRMApplication()
        {
            Pages.CRMLogin.navigatetoTheCRMApplication();
                       
        }

        [When(@"I login into Sekure CRM application")]
        public void WhenIEnterValidCredentialsToLogin(Table table)
        {
            String CRMusername = table.Rows[0]["CRMUserName"];
            String CRMpassword = table.Rows[0]["CRMPassword"];
            Pages.CRMLogin.sendLoginDetailsOnCRMLoginPage(CRMusername, CRMpassword);

        }

        [When(@"I navigate to Pending Email Page")]
        public void WhenINavigateToPendingEmailPage()
        {
            Pages.CRMPendingEmail.openPendingEmailPageafterCRMLogin();
        }

        [When(@"I search customer for which Unable to Assess request submitted")]
        public void WhenISearchCustomerForWhichUnableToAssessRequestSubmitted()
        {
            Pages.CustomerPage.searchtheCompanyForwhichUnabletoAssessRequestSent();
            
        }

        [Then(@"I should see customer in the list for which Unable to Assess request submitted")]
        public void ThenIShouldSeeCustomerInTheListForWhichUnableToAssessRequestSubmitted()
        {
            Boolean CompanyNameB = Pages.CustomerPage.checkcorrectCompanyNameDisplayedonPendingEmail();
            Assert.IsTrue(CompanyNameB, "Company Name is showing correctly on CRM Pending Email Page. ");
            
        }

        
    }
}
