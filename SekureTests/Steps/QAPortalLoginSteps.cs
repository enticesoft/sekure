﻿using SekureFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SekureTests.Steps
{
    [Binding]
    public class QAPortalLoginSteps
    {
        [Given(@"I am on QA Outsourcing Portal Login page")]
        public void GivenIAmOnQAOutsourcingPortalLoginPage()
        {
            Browser.NavigateTo();
        }

        [When(@"I enter valid UserName & Password")]
        public void WhenIEnterGuestQuantityDetails(Table table)
        {
            //Pages.LoginPage.logoutFromQAPortal();
            String Username = table.Rows[0]["UserName"];
            String password = table.Rows[0]["Password"];
            Pages.LoginPage.sendLoginDetailsOnLoginPage(Username, password);
        }

        [When(@"I click on the Login button")]
        public void WhenIClickOnTheLoginButton()
        {
           Pages.LoginPage.clickOnLoginButton();
        }

        [Then(@"I should login into QA Outsourcing Portal")]
        public void ThenIShouldLoginIntoQAOutsourcingPortal()
        {
            Boolean UserLoginDone = Pages.LoginPage.checkCustomersPageDisplayed();
            Assert.IsTrue(UserLoginDone, "User see Customers Page After Successfull Login into QA Portal. ");
        }

        [Then(@"I should logout from application")]
        public void ThenIShouldLogoutFromApplication()
        {
            Boolean UserLogoutDone = Pages.LoginPage.checkLogoutPageDisplayed();
            Assert.IsTrue(UserLogoutDone, "User Logout Successfully from QA Portal.");
        }

        [Then(@"I should see Invalid username or password message")]
        public void ThenIShouldSeeInvalidUsernameOrPasswordMessage()
        {
            Boolean ErrorwhileLogin = Pages.LoginPage.checkErrormessageDisplayed();
            Assert.IsTrue(ErrorwhileLogin, "User see error message that Username & Password are not valid.");
        }

        [When(@"I logout from the QA Outsourcing Portal")]
        public void WhenILogoutFromTheQAOutsourcingPortal()
        {
            Boolean UserLogoutDone = Pages.LoginPage.checkLogoutPageDisplayed();
            Assert.IsTrue(UserLogoutDone, "User Logout Successfully from QA Portal.");
        }




    }
}
