﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SekureFramework
{
    public static class ReadEmail
    {
        private static T GetReadEmail<T>() where T : new()
        {
            var reademail = new T();
            IWebDriver driver = Browser._Driver;
            PageFactory.InitElements(Browser.Driver, reademail);

            return reademail;
        }

        public static ReceiveEmail CheckEmail
        {
            get { return GetReadEmail<ReceiveEmail>(); }
        }
    }
}
