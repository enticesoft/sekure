﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EAGetMail;
using NUnit.Framework;

namespace SekureFramework
{
    public class ReceiveEmail
    {
        public TimeSpan startTime;
        public TimeSpan endTime;
        public TimeSpan elapsed;
        public double totalSeconds;
        public int elapsedInSecs;
        public int tCounter;
        public String exitWhile;
        public Boolean emailFlag;

        public Boolean CheckWelcomeEmail(String timeCounter)
        {
            
            try
            {
                // Gmail IMAP4 server is "imap.gmail.com"
                MailServer oServer = new MailServer("imap.gmail.com",
                                "gwr.nextgenqa1@gmail.com",
                                "$Reset123$",
                                ServerProtocol.Imap4);

                // Enable SSL connection.
                oServer.SSLConnection = true;

                // Set 993 SSL port
                oServer.Port = 993;

                MailClient oClient = new MailClient("TryIt");
                oClient.Connect(oServer);

                exitWhile = "False";
                emailFlag = false;
                tCounter = Int32.Parse(timeCounter);

                DateTime now = DateTime.Now;
                startTime = now.TimeOfDay;

                while(exitWhile.Equals("False"))
                {

                    // retrieve unread/new email only
                    oClient.GetMailInfosParam.Reset();
                    oClient.GetMailInfosParam.GetMailInfosOptions = GetMailInfosOptionType.NewOnly;

                    MailInfo[] infos = oClient.GetMailInfos();
                    //Console.WriteLine("Total {0} unread email(s)\r\n", infos.Length);
                    for (int i = 0; i < infos.Length; i++)
                    {
                        MailInfo info = infos[i];
                        //Console.WriteLine("Index: {0}; Size: {1}; UIDL: {2}",info.Index, info.Size, info.UIDL);

                        // Receive email from IMAP4 server
                        Mail oMail = oClient.GetMail(info);

                        //Console.WriteLine("From: {0}", oMail.From.ToString());
                        //Console.WriteLine("Subject: {0}\r\n", oMail.Subject);

                        if (oMail.From.ToString().Contains("Great Wolf Lodge") && oMail.Subject.Contains("Welcome from Great Wolf Lodge"))
                        {
                            emailFlag = true;
                            oClient.MarkAsRead(info, true);
                            oClient.Delete(info);
                            exitWhile = "True";
                            break;
                        }
                    }

                    Thread.Sleep(5000);
                    DateTime now1 = DateTime.Now;
                    endTime = now1.TimeOfDay;
                    elapsed = endTime.Subtract(startTime);
                    totalSeconds = elapsed.TotalSeconds;
                    elapsedInSecs = Convert.ToInt32(totalSeconds);
                    
                    if(elapsedInSecs >= tCounter)
                    {
                        exitWhile = "True";
                    }

                }

                // Quit and expunge emails marked as deleted from IMAP4 server.
                oClient.Quit();
                //Console.WriteLine("Completed!");
            }
            catch (Exception ep)
            {
                Console.WriteLine(ep.Message);
            }

            return emailFlag;
        }
    }
}
