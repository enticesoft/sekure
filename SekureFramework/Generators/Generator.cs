﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SekureFramework
{
    public static class Generator
    {
        private static T GetGenerator<T>() where T : new()
        {
            var generator = new T();
            IWebDriver driver = Browser._Driver;
            PageFactory.InitElements(Browser.Driver, generator);

            return generator;
        }

        public static EmailAddressGenerator EmailAddress
        {
            get { return GetGenerator<EmailAddressGenerator>(); }
        }
    }
}
