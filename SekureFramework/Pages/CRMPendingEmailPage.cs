﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SekureFramework
{
    public class CRMPendingEmailPage
    {
        [FindsBy(How = How.XPath, Using = "//*[@id='navigation-1']//*[@title='Reports']")]
        public IWebElement reportMenu;

        [FindsBy(How = How.XPath, Using = "//*[@id='navigation-1']//*[@title='Pending Emails']")]
        public IWebElement pendingEmailReport;

        [FindsBy(How = How.XPath, Using = "//button[@id='btn_search']")]
        public IWebElement searchBtnPendingEmail;


        public void openPendingEmailPageafterCRMLogin()
        {
            Browser.waitForElementToBeClickable(reportMenu, 20);
            reportMenu.Click();

            Browser.waitForElementToBeClickable(pendingEmailReport, 20);
            pendingEmailReport.Click();

            Browser.waitForElementToBeClickable(searchBtnPendingEmail, 15);
            searchBtnPendingEmail.Click();
        }
    }
}
