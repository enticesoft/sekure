﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SekureFramework
{
    public class CRMLoginPage
    {
        [FindsBy(How = How.XPath, Using = "//input[@placeholder='Username']")]
        public IWebElement CRMUsernametext;

        [FindsBy(How = How.XPath, Using = "//input[@id='password']")]
        public IWebElement CRMPasswordtext;

        [FindsBy(How = How.XPath, Using = "//input[@type='submit']")]
        public IWebElement CRMLoginBtn;

        public void navigatetoTheCRMApplication()
        {
            Browser.NavigateToCRM();
        }

        public void sendLoginDetailsOnCRMLoginPage(String UserName1, String Password1)
        {
            Browser.waitForElementToDisplayed(CRMUsernametext, 15);
            CRMUsernametext.SendKeys(UserName1);

            Browser.waitForElementToDisplayed(CRMPasswordtext, 15);
            CRMPasswordtext.SendKeys(Password1);

            CRMLoginBtn.Click();
        }
    }
}
