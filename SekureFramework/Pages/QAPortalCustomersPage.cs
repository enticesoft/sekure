﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SekureFramework
{
    public class QAPortalCustomersPage
    {

        public static string readOpenerName;
        public static string readOpenerNameonDetails;
        public static string readCustomerDetails;
        public static string CRMURL;

        [FindsBy(How = How.XPath, Using = "//tr[1]//td[3]")]
        public IWebElement openerName;

        [FindsBy(How = How.XPath, Using = "//tr[1]//td[2]")]
        public IWebElement customerName;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'customer-info')]/descendant::p[last()]")]
        public IWebElement openerNameOnDetailsPage;

        [FindsBy(How = How.XPath, Using = "//span[contains(text(),' Customer List ')]")]
        public IWebElement customerListBtn;


        [FindsBy(How = How.XPath, Using = "//*[@class='toolbar-items']//h1[contains(text(),'Customers')]")]
        public IWebElement customerListPage;

        [FindsBy(How = How.XPath, Using = "//*[@type='button']//span[contains(text(),'Unable to Assess')]")]
        public IWebElement unableToAssessBtn;

        [FindsBy(How = How.XPath, Using = "//textarea[starts-with(@id,'mat-input')]")]
        public IWebElement reasonTextBox;

        [FindsBy(How = How.XPath, Using = "//div[@class='mat-dialog-content']//span[contains(text(),' Send Email ')]")]
        public IWebElement sendEmailBtn;

        [FindsBy(How = How.XPath, Using = "//td[6]//input[1]")]
        public IWebElement searchCompanytextbox;

        [FindsBy(How = How.XPath, Using = "//table[@id='tablesorter']")]
        public IWebElement searchedCompany;
        
        String searchedCompany1 = "//table[@id='tablesorter']//td[contains(text(),'";
        String searchedCompany2 = "')]";

        public static string getComName;
        public IWebElement ComNameElement;
        public static string getComNameElementTxt;
        public static string getFinalCompanyName;
        public static string ComName;


        public Boolean checkcorrectCompanyNameDisplayedonPendingEmail()
        {
                
            return getFinalCompanyName.Equals(readCustomerDetails);
            
        }
               
        public void searchtheCompanyForwhichUnabletoAssessRequestSent()
        {
            Browser.waitForElementToBeClickable(searchCompanytextbox, 20);
            searchCompanytextbox.SendKeys(readCustomerDetails);
            
            getComName = searchedCompany1 + readCustomerDetails + searchedCompany2;
            ComNameElement = searchedCompany.FindElement(By.XPath(getComName));

            getComNameElementTxt = ComNameElement.Text;
            getFinalCompanyName = getComNameElementTxt.Substring(19);
        }

        public void clickOnOpenerNameLink()
        {
            Browser.waitForElementToBeClickable(openerName, 20);
            readOpenerName = Browser.getText(openerName);
            readCustomerDetails = Browser.getText(customerName);
            openerName.Click();
        }

        public Boolean checkcorrectOpenerNameDisplayed()
        {
                  
            Browser.waitForElementToDisplayed(openerNameOnDetailsPage, 10);
            readOpenerNameonDetails = Browser.getText(openerNameOnDetailsPage);
            return readOpenerNameonDetails.Equals(readOpenerName);
        }
        public void clickOnCustomerListBtn()
        {
            Browser.waitForElementToBeClickable(customerListBtn, 20);
            customerListBtn.Click();
        }
        public Boolean checkCustomerListPageDisplayed()
        {
            Browser.waitForElementToDisplayed(customerListPage, 10);
            bool customerListPageopen = customerListPage.Displayed;
            return customerListPageopen;
        }

        public void clickOnUnabletoAssessBtn()
        {
            Browser.waitForElementToBeClickable(unableToAssessBtn, 20);
            unableToAssessBtn.Click();
           
        }

        public void enterReasononthePopup(String Reason)
        {
            Browser.waitForElementToDisplayed(reasonTextBox, 15);
            reasonTextBox.SendKeys(Reason);
            
        }

        public void clickOnSendEmailBtn()
        {
            Browser.waitForElementToBeClickable(sendEmailBtn, 20);
            sendEmailBtn.Click();

        }
     
    }
}
