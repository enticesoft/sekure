﻿using SekureFramework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using TechTalk.SpecFlow;

namespace SekureFramework
{
    public static class Pages
    {

       // public static IWebDriver _driver;

        private static T GetPage<T>() where T : new()
        {
            var page = new T();
            IWebDriver driver = Browser._Driver;
            PageFactory.InitElements(Browser.Driver, page);

            return page;
        }

        public static TopNavigations TopNavigation
        {
            get { return GetPage<TopNavigations>(); }
        }
        public static QAPortalLoginPage LoginPage
        {
            get { return GetPage<QAPortalLoginPage>(); }
        }

        public static QAPortalCustomersPage CustomerPage
        {
            get { return GetPage<QAPortalCustomersPage>(); }
        }

        public static CRMLoginPage CRMLogin
        {
            get { return GetPage<CRMLoginPage>(); }
        }

        public static CRMPendingEmailPage CRMPendingEmail
        {
            get { return GetPage<CRMPendingEmailPage>(); }
        }
    }
}
