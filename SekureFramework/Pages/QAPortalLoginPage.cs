﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using SekureFramework.TestData;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using AventStack.ExtentReports;
using System.Diagnostics.CodeAnalysis;

namespace SekureFramework
{
    public class QAPortalLoginPage
    {
        [FindsBy(How = How.XPath, Using = "//input[@type='text']")]
        public IWebElement userNameOnLogin;

        [FindsBy(How = How.XPath, Using = "//input[@type='password']")]
        public IWebElement passwordOnLogin;

        [FindsBy(How = How.XPath, Using = "//button[@class='btnSubmit']")]
        public IWebElement loginButton;

        [FindsBy(How = How.XPath, Using = "//*[@class='mat-toolbar mat-elevation-z6 mat-primary mat-toolbar-single-row']//h1[contains(text(),'Customers')]")]
        public IWebElement CustomersPage;

        [FindsBy(How = How.XPath, Using = "//*[@class='mat-toolbar mat-elevation-z6 mat-primary mat-toolbar-single-row']//div[@class='right-section']//p[contains(text(),'Logout')]")]
        public IWebElement logoutLink;

        [FindsBy(How = How.XPath, Using = "//*[@class='page-header logged-out']//small[contains(text(),'You are now logged out')]")]
        public IWebElement logoutpage;

        [FindsBy(How = How.XPath, Using = "//div[@class='danger validation-summary-errors']//li[contains(text(),'Invalid username or password')]")]
        public IWebElement loginError;


        public void sendLoginDetailsOnLoginPage(String UserName, String Password)
        {
            Browser.waitForElementToDisplayed(userNameOnLogin, 15);
            userNameOnLogin.SendKeys(UserName);
            passwordOnLogin.SendKeys(Password);
        }

        public void clickOnLoginButton()
        {
            Browser.waitForElementToBeClickable(loginButton, 10);
            loginButton.Click();
        }
        public Boolean checkCustomersPageDisplayed()
        {
            Browser.waitForElementToDisplayed(CustomersPage, 10);
            bool LoginSucessfully  = CustomersPage.Displayed;
            return LoginSucessfully;
        }
        public Boolean checkLogoutPageDisplayed()
        {
            Browser.waitForElementToBeClickable(logoutLink, 10);
            logoutLink.Click();
            bool LogoutSucessfully = logoutpage.Displayed;
            return LogoutSucessfully;
        }

        public void logoutFromQAPortal()
        {
            if(logoutLink.Exists())
            { 
                Browser.waitForElementToBeClickable(logoutLink, 10);
                logoutLink.Click();
                Browser.NavigateTo();
            }
        }

        public Boolean checkErrormessageDisplayed()
        {
            Browser.waitForElementToDisplayed(loginError, 10);
            bool LoginError = loginError.Displayed;
            return LoginError;
        }
    }
}
