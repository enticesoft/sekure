﻿using System;
using System.Configuration;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;

namespace SekureFramework
{
    public class Browser
    {
        public IWebDriver WebDriver { get; set; }
        public string environmentURL { get; set; }

        public static IWebDriver webDriver;

        private static string baseUrl = "https://app-outsourcing-qa.azurewebsites.net"; //use app config for determining environment

        //public static IWebDriver webDriver = new ChromeDriver();
        //  public static IWebDriver webDriver = new InternetExplorerDriver();
        //public static IWebDriver webDriver = new FirefoxDriver();

        public Browser(IWebDriver webDriver)
        {
            environmentURL = ConfigurationManager.AppSettings["Base_URL"];
            WebDriver = webDriver;
        }

        public static void Initialize()
        {

            Goto("");
        }

        public static void LaunchBrowser()
        {
            string browser = ConfigurationManager.AppSettings["Browser"];

            switch (browser)
            {
                case "Firefox":
                    webDriver = new FirefoxDriver();
                    break;
                case "Chrome":
                    webDriver = new ChromeDriver();
                    break;
                case "IE":
                    webDriver = new InternetExplorerDriver();
                    break;
                case "Edge":
                    webDriver = new EdgeDriver();
                    break;
            }

        }

        public static void NavigateTo()
        {
            webDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["Base_URL"]);
            webDriver.Manage().Window.Maximize();

        }

        public static void NavigateToCRM()
        {
            webDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["Base_URL_CRM"]);
            webDriver.Manage().Window.Maximize();

        }

        public static string Title
        {
            get { return webDriver.Title; }
        }

        public static string CurrentURL
        {
            get { return webDriver.Url; }
        }

        public static ISearchContext Driver
        {
            get { return webDriver; }
        }

        public static IWebDriver _Driver
        {
            get { return webDriver; }
        }

        public static IWebDriver driver
        {
            get { return webDriver; }
        }

        public static void Goto(string url)
        {
            webDriver.Url = baseUrl + url;
        }

        public static void Close()
        {
            webDriver.Close();
            webDriver.Quit();
            webDriver.Dispose();
        }

        public static void waitForImplicitTime(int waitTime)
        {

            //IWebDriver driver = new ChromeDriver();
            webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(waitTime);
        }

        public static void waitForElementToBeClickable(IWebElement element, int waitTime)
        {
           
            WebDriverWait wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(waitTime));
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public static void waitForElementToDisplayed(IWebElement element, int waitTime)
        {
            for (int i = 0; i < waitTime; i++)
            {
                if(element.Exists())
                {
                    break;
                }
                else
                {
                    Thread.Sleep(1000);
                }
            }

        }

        public static string getText(IWebElement element)
        {
            var text = element.Text;
            return text;
        }

        public static string getTextboxValue(IWebElement element)
        {
            var text = element.GetAttribute("value");
            return text;
        }

        public static void clearTextboxValue(IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            js.ExecuteScript("arguments[0].value='';", element);

        }

        public static void alert()
        {
            IAlert simpleAlert = webDriver.SwitchTo().Alert();
            simpleAlert.Accept();
        }

        
        public static void scrollToAnElement(IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            js.ExecuteScript("arguments[0].scrollIntoView();", element);
        }

        public static void scrollDownToBottomOfPage()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
        }

        public static double stringToDoubleConvert(IWebElement element)
        {
            waitForElementToDisplayed(element, 10);

            String value = getText(element);

            value = value.Substring(1);

            double convertVal = Convert.ToDouble(value);

            return convertVal;
        }


    }
}