﻿using Dapper;
using System.Data.OleDb;
using System.Linq;
using System;
using xl = Microsoft.Office.Interop.Excel;
using System.IO;

namespace SekureFramework.TestData
{
    public class DataAccess
    {
        public static String getData(int col, int row)
        {
            String value;

            var dir = AppDomain.CurrentDomain.BaseDirectory.Replace("\\SekureTests\\bin\\Debug", "");
            var fileName = dir + "\\SekureFramework\\TestData\\TestData.xlsx";

            xl.Application x1app = new xl.Application();
            xl.Workbook x1Wb = x1app.Workbooks.Open(fileName);
            //excel.Workbook x1Wb = x1app.Workbooks.Open(@"D:\TestDataFile.xlsx");
            xl.Worksheet x1Ws = x1Wb.Sheets[1];
            xl.Range x1range = x1Ws.UsedRange;
            // value = x1range.Cells[row][col].value2;
            value = Convert.ToString(x1range.Cells[col][row].value2);
            x1Wb.Close();
            x1app.Quit();
            return value;
        }

        public static String putData(String resID)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory.Replace("\\SekureTests\\bin\\Debug", "");
            var fileName = dir + "\\SekureFramework\\TestData\\ReservationData.txt";
            File.AppendAllText(fileName, resID + ",");
            return resID;
        }

    }
 }
